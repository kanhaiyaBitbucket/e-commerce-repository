package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.BookCategory;

public interface BookCategoryRepository extends JpaRepository<BookCategory, Long> {
	
	@Query("SELECT new com.bookstore.dto.CategoryWithId(b.id,b.categoryName) FROM BookCategory b")
	public List<CategoryWithId> findAllWithoutBookId();
	
	@Query("SELECT new com.bookstore.dto.CategoryWithId(b.id,b.categoryName) FROM ShopCategory b")
	public List<CategoryWithId> findAllWithoutshopId();

	

}
