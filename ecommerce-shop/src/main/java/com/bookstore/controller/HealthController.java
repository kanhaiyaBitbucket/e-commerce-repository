package com.bookstore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.Electronics;
import com.bookstore.entity.Health;
import com.bookstore.repository.HealthCategoryRepository;
import com.bookstore.repository.HealthRepository;

@RestController
@CrossOrigin(origins = "*")
public class HealthController {

	@Autowired
	private HealthRepository healthRepository;
	@Autowired
	private HealthCategoryRepository healthCategoryRepository;
	
	@GetMapping("/healthcategories")
	public List<CategoryWithId> getHelthCategories() {
		return healthCategoryRepository.findAllWithoutHealthId();
	}
	
	@GetMapping("/healths")
	public List<Health> helths() {
		return healthRepository.findAll();
	}
	
	@PostMapping("/healths")
	public void saveHealth(@RequestBody Health health) {
		healthRepository.save(health);
	}
	
	@GetMapping("/healths/{id}")
	public List<Health> healthsByHelthCategory(@PathVariable("id") Long id) {
		return healthRepository.findHealthsByHealthCategoryId(id);
	}
	
	@GetMapping("/health/{key}")
	public List<Health> healthsBySearchKey(@PathVariable("key") String key) {
		return healthRepository.searchHealth(key);
	}
	
	@GetMapping("/gethealth/{id}")
	public Optional<Health> getHealth(@PathVariable("id") Long id) {
		return healthRepository.findById(id);
	}
}
