package com.bookstore.repository;

import java.util.List;

import org.hibernate.type.TrueFalseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.entity.Cart;

public interface CartRepository extends JpaRepository<Cart, Long> {

	@Query(value = "select * from cart where customer_id = ?1", nativeQuery = true)
	public List<Cart> findAllById(Long id);
}
