import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { BookCategory } from 'src/app/commom/BookCategory';
import { ClothService } from 'src/app/services/cloth.service';
import { ElectronicsService } from 'src/app/services/electronics.service';
import { HealthService } from 'src/app/services/health.service';
import { HomeapplService } from 'src/app/services/homeappl.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';
import { CartService } from 'src/app/services/cart.service';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  //Getting all refrences of services

  constructor(private bookser: BookService,private routes: ActivatedRoute,
               private clothser: ClothService, private electronicserv: ElectronicsService,
               private healthserv: HealthService, private homeapplser: HomeapplService,
               private router:Router, private genserv: GeneralService,
               private cartServ: CartService) { }
 
  
  Categorys:BookCategory[];
  items:BookCategory[];
  suburl:string;
  key:string;
  subUrl:any;
  item:any[];
  isSubUrlValid:boolean = false;
  uid:number;
  noOfItemInCart:any;
  cartItem:any;
  
  ngOnInit() {

    // Injecting item inside suburl
    
    this.bookser.suburl.subscribe((data)=>{
      this.subUrl = data;
    })

    // Getting item category from database

    this.bookser.getShopCategory().subscribe((data)=>{
      this.items = data;
    });

    this.routes.paramMap.subscribe((parm)=>{
        this.uid = Number(parm.get('uid'));
    });

    this.cartServ.getItems(this.uid).subscribe((data)=>{
      this.cartItem = data;
      this.noOfItemInCart = this.cartItem.length;
    });

   
  }

  // Changeing url when user click on search
  onSearch(){
    this.isSubUrlValid = true;
    this.router.navigate(["/items",this.uid,"search",this.subUrl,this.key,this.uid]);
  }

  // onClick method to fetch the data from database based upon shop id

  onClick(id:number,itemName:string){
      switch(id){
        case 1 :
          this.suburl = itemName;
          this.bookser.getBookCategory().subscribe((data)=>{
            this.Categorys=data
          });
          break;
        case 2:
          this.suburl = itemName;
          this.clothser.getClothCategory().subscribe((data)=>{
            this.Categorys = data;
          });
          break;
        case 3:
          this.suburl = itemName;
          this.electronicserv.getElectronicCategory().subscribe((data)=>{
            this.Categorys = data;
          });
          break;
        case 4:
          this.suburl = "homeappls";
          this.homeapplser.getHomeApplsCategory().subscribe((data)=>{
            this.Categorys = data;
          });
          break;
        case 5:
          this.suburl = itemName;
          this.healthserv.getHelthsCategory().subscribe((data)=>{
            this.Categorys = data;
          });
          break;
      }

  }
  


}
