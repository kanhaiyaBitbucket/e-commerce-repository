import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../commom/book';
import { BookCategory } from '../commom/BookCategory';


@Injectable({
  providedIn: 'root'
})
export class ElectronicsService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "http://localhost:8080/electronics";
  private bookCategoryUrl:string = "http://localhost:8080/electroniccategories";

// method for  Geting all Electronics

  getElectronics():Observable<Book[]>{
    return this.http.get<Book[]>(this.baseUrl)
  }

// method for  Geting all ElectronicsCategory
  
  getElectronicCategory():Observable<BookCategory[]>{
    return this.http.get<BookCategory[]>(this.bookCategoryUrl)
  }

//  method for  Geting  Electronics based upon ElectronicsCategory_id

  getElectronicsById(id):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/electronics/" + id);
  }

 //  method for  Geting  Electronics based upon search_key 

  getElectronicsByKey(key):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/electronic/" + key);
  }

  //  method for  Geting  Electronics based upon id

  getElectronicById(id):Observable<Book>{
    return this.http.get<Book>("http://localhost:8080/getelectronic/" + id);
  }

  saveElectronic(electronic){
    this.http.post(this.baseUrl,electronic).subscribe((data)=>{ })
}

}
