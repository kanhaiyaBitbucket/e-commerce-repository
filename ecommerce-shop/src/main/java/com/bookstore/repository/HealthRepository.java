package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.entity.Electronics;
import com.bookstore.entity.Health;

public interface HealthRepository extends JpaRepository<Health, Long> {

	@Query(value = "select * from healths where category_id = ?1", nativeQuery = true)
	public List<Health> findHealthsByHealthCategoryId (Long id);
	
	@Query("from Health where upper(description) like concat('%',upper(?1),'%')")
	public List<Health> searchHealth(String input);
}
