package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.entity.Book;
import com.bookstore.entity.Cloth;

public interface ClothRepository extends JpaRepository<Cloth, Long> {

	@Query(value = "select * from cloths where category_id = ?1", nativeQuery = true)
	public List<Cloth> findClothByClothCategoryId (Long id);
	
	@Query("from Cloth where upper(description) like concat('%',upper(?1),'%')")
	public List<Cloth> searchCloths(String input);
}
