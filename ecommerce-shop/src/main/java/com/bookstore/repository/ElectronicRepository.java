package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.entity.Book;
import com.bookstore.entity.Electronics;

public interface ElectronicRepository extends JpaRepository<Electronics, Long> {

	@Query(value = "select * from electronics where category_id = ?1", nativeQuery = true)
	public List<Electronics> findElectronicByElectronicCategoryId (Long id);
	
	@Query("from Electronics where upper(description) like concat('%',upper(?1),'%')")
	public List<Electronics> searchElectronics(String input);
}
