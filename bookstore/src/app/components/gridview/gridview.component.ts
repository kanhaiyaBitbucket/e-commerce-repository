import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { ActivatedRoute } from '@angular/router';
import { ClothService } from 'src/app/services/cloth.service';
import { ElectronicsService } from 'src/app/services/electronics.service';
import { HealthService } from 'src/app/services/health.service';
import { HomeapplService } from 'src/app/services/homeappl.service';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from 'src/app/services/general.service';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-gridview',
  templateUrl: './gridview.component.html',
  styleUrls: ['./gridview.component.css']
})
export class GridviewComponent implements OnInit {

//Getting all refrences of services

  constructor(private bookser: BookService,private routes: ActivatedRoute,
    private clothser: ClothService, private electronicserv: ElectronicsService,
    private healthserv: HealthService, private homeapplser: HomeapplService,
    private http: HttpClient, private genserv: GeneralService,
    private cartserv: CartService) { }

  id:number;
  items:any;
  suburl:string;
  item:string;
  test:any;
  uid:number;
  cartItems:any;
  height:number;
  width:number;

  ngOnInit() {

 // Getting the id  from url
 // Getting the item  from url and calling the call function

    this.routes.paramMap.subscribe((param)=>{
      this.uid = Number(param.get('uid'))
      this.id = Number(param.get('id'));
      this.item = param.get('item');
      this.bookser.suburl.next(this.item);
      if(this.item == 'cloths'){
           this.height = 400;
           this.width = 150;
      }else{
        this.width = 150;
        this.height = 260;
      }
      this.call();
   });

   
  }

  cartItem = {
    imgUrl: "",
    product: "",
    available:"",
    quantity: 0,
    price: 0.0,
    customer_id:0,
    itemType:"",
    itemId: 0
  }

  

  baseUrl:string = "http://localhost:8080/cart";
  
  // call method to fetch the data from database based upon id

  call(){

    switch(this.item){

      // call method to fetch the data from database based upon id from book table

      case "books":
        this.items = [];
        this.bookser.getBooksById(this.id).subscribe((books)=>{
          this.items = books;
        });
        break;

        // call method to fetch the data from database based upon id from cloth table

      case "cloths":
        this.items = [];
        this.clothser.getClothsById(this.id).subscribe((cloths)=>{
          this.items = cloths;
        });
        break;

        // call method to fetch the data from database based upon id from electronics table

      case "electronics":
        this.items = [];
        this.electronicserv.getElectronicsById(this.id).subscribe((electronics)=>{
          this.items = electronics;
        }) 
         break;

         // call method to fetch the data from database based upon id from health table

      case "healths":
        this.items = [];
        this.healthserv.getHelthsById(this.id).subscribe((healths)=>{
          this.items = healths;
        });
        break;

        // call method to fetch the data from database based upon id from homeappls table

      case "homeappls": 
        this.items = [];  
        this.homeapplser.getHomeApplsById(this.id).subscribe((homeappls)=>{
          this.items = homeappls;
        });
        break;  
    }
    
  }

  addInCart(item){
     this.cartItem.imgUrl = item.imageUrl;
     if(item.unitsInStock >0){
          this.cartItem.available = "In stock";
     }else{
      this.cartItem.available = "out of stock";
     }
     this.cartItem.price = item.unitPrice;
     this.cartItem.product = item.name;
     this.cartItem.quantity = 1;
     this.cartItem.customer_id = this.uid;
     this.cartItem.itemId = item.id;

     switch(this.item){

      case "books":
          this.cartItem.itemType = "books";
        break;

      case "cloths":
         this.cartItem.itemType = "cloths";
        break;

      case "electronics":
        this.cartItem.itemType = "electronics";
         break;

      case "healths":
        this.cartItem.itemType = "healths";
        break;

      case "homeappls": 
      this.cartItem.itemType = "homeappls";
        break;  
    }
     
     
    this.http.post(this.baseUrl,this.cartItem).subscribe((data)=>{
      
    });
    
  }


}
