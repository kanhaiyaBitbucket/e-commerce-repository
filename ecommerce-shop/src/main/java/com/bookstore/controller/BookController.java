package com.bookstore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.Book;
import com.bookstore.entity.BookCategory;

import com.bookstore.repository.BookCategoryRepository;
import com.bookstore.repository.BookRepository;

@RestController
@CrossOrigin(origins = "*")
public class BookController {
	@Autowired
	private BookCategoryRepository bookCategoryRepository;
	
	@Autowired
	private BookRepository repo;
    
	@GetMapping("/bookcategories")
	public List<CategoryWithId> getBookCategories() {
		return bookCategoryRepository.findAllWithoutBookId();
	}
	
	@GetMapping("/shopcategories")
	public List<CategoryWithId> getShopCategories() {
		return bookCategoryRepository.findAllWithoutshopId();
	}
	
	@GetMapping("/books")
	public List<Book> books() {
		return repo.findAll();
	}
	
	@PostMapping("/books")
	public void saveBook(@RequestBody Book book) {
		 repo.save(book);
	}
	
	@GetMapping("/books/{id}")
	public List<Book> booksByBookCategory(@PathVariable("id") Long id) {
		return repo.findBookByBookCategory(id);
	}
	
	@GetMapping("/book/{key}")
	public List<Book> booksBySearchKey(@PathVariable("key") String key) {
		return repo.searchBook(key);
	}
	
	@GetMapping("/getbook/{id}")
	public Optional<Book> getBook(@PathVariable("id") Long id) {
		return repo.findById(id);
	}
}
