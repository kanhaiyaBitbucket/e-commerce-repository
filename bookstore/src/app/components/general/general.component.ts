import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';
import { ClothService } from 'src/app/services/cloth.service';
import { BookService } from 'src/app/services/book.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {

  constructor(private Cloth:ClothService,private routes:ActivatedRoute, 
              private bookser: BookService) { }

  uid:number;
  items:any;
 

  ngOnInit() {
    this.routes.paramMap.subscribe((parm)=>{
      this.uid = Number(parm.get('uid'));
    })

    this.Cloth.getCloths().subscribe((data)=>{
      this.items = data;
    })

  }

}
