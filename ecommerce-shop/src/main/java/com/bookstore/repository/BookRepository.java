package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.entity.Book;


public interface BookRepository extends JpaRepository<Book, Long> {
	@Query(value = "select * from books where category_id = ?1", nativeQuery = true)
	public List<Book> findBookByBookCategory (Long id);
		
	@Query("from Book where upper(description) like concat('%',upper(?1),'%')")
	public List<Book> searchBook(String input);

}
