import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from '../commom/customer';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "http://localhost:8080/users";

  // To get all users

  getUsers():Observable<Customer[]>{
    return this.http.get<Customer[]>(this.baseUrl);
  }

  // To get user based upon username and password

  getUser(email,password):Observable<Customer>{
    return this.http.get<Customer>(`http://localhost:8080/users/${email}/${password}`);
  }

}
