package com.bookstore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.Cloth;
import com.bookstore.entity.Electronics;
import com.bookstore.repository.ElectronicCategoryRepository;
import com.bookstore.repository.ElectronicRepository;

@RestController
@CrossOrigin(origins = "*")
public class ElectronicController {

	@Autowired
	private ElectronicRepository electronicRepository;
	@Autowired
	private ElectronicCategoryRepository electronicCategoryRepository;
	
	@GetMapping("/electroniccategories")
	public List<CategoryWithId> getElectronicCategories() {
		return electronicCategoryRepository.findAllWithoutElectronicId();
	}
	
	@GetMapping("/electronics")
	public List<Electronics> electronics() {
		return electronicRepository.findAll();
	}
	
	@PostMapping("/electronics")
	public void saveElectronic(@RequestBody Electronics electronic) {
		electronicRepository.save(electronic);
	}
	
	@GetMapping("/electronics/{id}")
	public List<Electronics> electronicsByElectronicCategory(@PathVariable("id") Long id) {
		return electronicRepository.findElectronicByElectronicCategoryId(id);
	}
	
	@GetMapping("/electronic/{key}")
	public List<Electronics> electronicsBySearchKey(@PathVariable("key") String key) {
		return electronicRepository.searchElectronics(key);
	}
	
	@GetMapping("/getelectronic/{id}")
	public Optional<Electronics> getElectronics(@PathVariable("id") Long id) {
		return electronicRepository.findById(id);
	}
}
