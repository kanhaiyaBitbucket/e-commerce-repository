package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.ClothCategory;

public interface ClothCategoryRepository extends JpaRepository<ClothCategory, Long> {

	@Query("SELECT new com.bookstore.dto.CategoryWithId(c.id,c.categoryName) FROM ClothCategory c")
	public List<CategoryWithId> findAllWithoutClothId();
}
