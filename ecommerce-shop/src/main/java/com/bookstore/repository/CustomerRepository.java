package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.User;
import com.bookstore.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String>{
    
	@Query("select new com.bookstore.dto.User(c.email,c.password,c.id) from Customer c where email = ?1 and password = ?2")
	public User findByEmailAndPassword(String email, String password);
}
