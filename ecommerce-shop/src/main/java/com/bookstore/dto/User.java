package com.bookstore.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User {

	private String email;
	private String password;
	private Long id;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public User() {
		super();
	}
	public User(String email, String password, Long id) {
		super();
		this.email = email;
		this.password = password;
		this.id = id;
	}
	
	
	
	
	
	
}
