import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/services/registration.service';
import { Customer } from 'src/app/commom/customer';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private regserv: RegistrationService, private router: Router, private genserv: GeneralService) { }

  username:string;
  password:string;
  users:Customer = new Customer();
  invalid:boolean = false;
  
  

  ngOnInit() {
    
  }

  getData(){
    this.regserv.getUser(this.username,this.password).subscribe((data)=>{
      this.users = data;
    });
  }

  onLogin(){
    if(this.users == null || this.users.password == undefined){
      this.invalid = true;
    }else{
      
        this.router.navigate(["/items",this.users.id]);
     
    }
     
  }

  

}
