import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../commom/book';
import { BookCategory } from '../commom/BookCategory';


@Injectable({
  providedIn: 'root'
})
export class ClothService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "http://localhost:8080/cloths";
  private bookCategoryUrl:string = "http://localhost:8080/clothcategories";

// method for  Geting all cloths

  getCloths():Observable<Book[]>{
    return this.http.get<Book[]>(this.baseUrl)
  }

// method for  Geting all ClothCategory

  getClothCategory():Observable<BookCategory[]>{
    return this.http.get<BookCategory[]>(this.bookCategoryUrl)
  }

  //  method for  Geting  Cloths based upon ClothCategory_id

  getClothsById(id):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/cloths/" + id);
  }

//  method for  Geting  Cloths based upon search_key

  getClothsByKey(key):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/cloth/" + key);
  }

//  method for  Geting  Cloth based upon id

  getClothById(id):Observable<Book>{
    return this.http.get<Book>("http://localhost:8080/getcloth/" + id);
  }

  saveCloth(cloth){
    this.http.post(this.baseUrl,cloth).subscribe((data)=>{ })
}
}
