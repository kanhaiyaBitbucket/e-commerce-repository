package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.ShopCategory;

public interface ShopCategoryRepository extends JpaRepository<ShopCategory, Long> {

	@Query("SELECT new com.bookstore.dto.CategoryWithId(s.id,s.categoryName) FROM ShopCategory s")
	public List<CategoryWithId> findAllWithoutshopId();
}
