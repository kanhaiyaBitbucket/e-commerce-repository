package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.HealthCategory;

public interface HealthCategoryRepository extends JpaRepository<HealthCategory, Long> {

	@Query("SELECT new com.bookstore.dto.CategoryWithId(h.id,h.categoryName) FROM HealthCategory h")
	public List<CategoryWithId> findAllWithoutHealthId();
}
