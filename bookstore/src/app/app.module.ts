import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { from } from 'rxjs';
import { BookService } from './services/book.service';
import { Routes,RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SearchComponent } from './components/search/search.component';
import { DetailedviewComponent } from './components/detailedview/detailedview.component';
import { GridviewComponent } from './components/gridview/gridview.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { GeneralComponent } from './components/general/general.component';
import { CartComponent } from './components/cart/cart.component';
import { OrderplacedComponent } from './components/orderplaced/orderplaced.component';


const routes:Routes = [
  {path:"", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"items/:uid", component: BookListComponent, children: [
     {path:"", component: GeneralComponent},
     {path:"get/:item/:id/:uid",component: GridviewComponent},
     {path:"search/:item/:key/:uid", component: SearchComponent},
     {path:"detail/:item/:id/:uid", component: DetailedviewComponent},
     {path:"cart/:uid", component: CartComponent},
     {path:"order", component: OrderplacedComponent}
  ] },
  {path:"**", component: PageNotFoundComponent}
  
]

@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    SearchComponent,
    DetailedviewComponent,
    GridviewComponent,
    LoginComponent,
    RegisterComponent,
    PageNotFoundComponent,
    GeneralComponent,
    CartComponent,
    OrderplacedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    BookService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
