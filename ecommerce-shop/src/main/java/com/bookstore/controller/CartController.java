package com.bookstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.entity.Cart;
import com.bookstore.repository.CartRepository;

@RestController
@CrossOrigin(origins = "*")
public class CartController {
	
	@Autowired
	private CartRepository cartRepository;
	
	@GetMapping("/cart/{id}")
	public List<Cart> getItems(@PathVariable("id") Long id){
		return cartRepository.findAllById(id);
	}
	
	@PutMapping("/cart")
	public void updateItem(@RequestBody Cart cart) {
		cartRepository.save(cart);
	}
	
	@PostMapping("/cart")
	public void setItem(@RequestBody Cart cart) {
		cartRepository.save(cart);
	}
	
	@DeleteMapping("/cart/{id}")
	public void deleteItem(@PathVariable("id") Long id) {
		cartRepository.deleteById(id);
	}

}
