package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.ElectronicCategory;

public interface ElectronicCategoryRepository extends JpaRepository<ElectronicCategory, Long> {

	@Query("SELECT new com.bookstore.dto.CategoryWithId(e.id,e.categoryName) FROM ElectronicCategory e")
	public List<CategoryWithId> findAllWithoutElectronicId();
}
