import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Book } from '../commom/book';
import { BookCategory } from '../commom/BookCategory';

@Injectable({
  providedIn: 'root'
})
export class BookService {
 suburl = new Subject();
 items = new Subject();
 subUser = new Subject();
  constructor(private http:HttpClient) { }

  private baseUrl:string = "http://localhost:8080/books";
  private bookCategoryUrl:string = "http://localhost:8080/bookcategories";
  private shopCategoryUrl:string = "http://localhost:8080/shopcategories";

// method for  Geting all homeappls

  getBooks():Observable<Book[]>{
    return this.http.get<Book[]>(this.baseUrl)
  }

// method for  Geting all BookCategory  

  getBookCategory():Observable<BookCategory[]>{
    return this.http.get<BookCategory[]>(this.bookCategoryUrl)
  }

// method for  Geting all shopCategory

  getShopCategory():Observable<BookCategory[]>{
    return this.http.get<BookCategory[]>(this.shopCategoryUrl)
  }

//  method for  Geting  Book based upon BookCategory_id

  getBooksById(id):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/books/" + id);
  }

//  method for  Geting  Book based upon search_key

  getBooksByKey(key):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/book/" + key);
  }

//  method for  Geting  Book based upon id

  getBookById(id):Observable<Book>{
    return this.http.get<Book>("http://localhost:8080/getbook/" + id);
  }

  saveBook(book){
      this.http.post(this.baseUrl,book).subscribe((data)=>{ })
  }

}
