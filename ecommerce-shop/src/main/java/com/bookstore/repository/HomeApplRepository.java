package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.entity.Health;
import com.bookstore.entity.HomeAppl;

public interface HomeApplRepository extends JpaRepository<HomeAppl, Long> {

	@Query(value = "select * from home_appl where category_id = ?1", nativeQuery = true)
	public List<HomeAppl> findHomeApplsByHomeCategoryId (Long id);
	
	@Query("from HomeAppl where upper(description) like concat('%',upper(?1),'%')")
	public List<HomeAppl> searchHomeAppl(String input);
}
