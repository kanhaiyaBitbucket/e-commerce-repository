export class Item{
	id:number;
	product:string;
	imgUrl:string;
	available:string;
	quantity:number;
	price:number;
	customer_id:number;
	itemType:string;
	itemId:number;
}