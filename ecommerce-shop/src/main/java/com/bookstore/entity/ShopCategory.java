package com.bookstore.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="shop_category")
public class ShopCategory {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="category_name")
	private String categoryName;
	
	@OneToMany(cascade=CascadeType.ALL, targetEntity = ClothCategory.class)
	@JoinColumn(name="shop_category_id", referencedColumnName = "id")
	private Set<Book> shop_cloth_categoryId;
	
	@OneToMany(cascade=CascadeType.ALL, targetEntity = BookCategory.class)
	@JoinColumn(name="shop_category_id", referencedColumnName = "id")
	private Set<Book> shop_book_categoryId;
	
	@OneToMany(cascade=CascadeType.ALL, targetEntity = ElectronicCategory.class)
	@JoinColumn(name="shop_category_id", referencedColumnName = "id")
	private Set<Book> shop_electronic_categoryId;
	
	@OneToMany(cascade=CascadeType.ALL, targetEntity = HealthCategory.class)
	@JoinColumn(name="shop_category_id", referencedColumnName = "id")
	private Set<Book> shop_health_CategoryId;
	
	@OneToMany(cascade=CascadeType.ALL, targetEntity = HomeApplCategory.class)
	@JoinColumn(name="shop_category_id", referencedColumnName = "id")
	private Set<Book> shop_homeappl_CategoryId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Set<Book> getShop_cloth_categoryId() {
		return shop_cloth_categoryId;
	}

	public void setShop_cloth_categoryId(Set<Book> shop_cloth_categoryId) {
		this.shop_cloth_categoryId = shop_cloth_categoryId;
	}

	public Set<Book> getShop_book_categoryId() {
		return shop_book_categoryId;
	}

	public void setShop_book_categoryId(Set<Book> shop_book_categoryId) {
		this.shop_book_categoryId = shop_book_categoryId;
	}

	public Set<Book> getShop_electronic_categoryId() {
		return shop_electronic_categoryId;
	}

	public void setShop_electronic_categoryId(Set<Book> shop_electronic_categoryId) {
		this.shop_electronic_categoryId = shop_electronic_categoryId;
	}

	public Set<Book> getShop_health_CategoryId() {
		return shop_health_CategoryId;
	}

	public void setShop_health_CategoryId(Set<Book> shop_health_CategoryId) {
		this.shop_health_CategoryId = shop_health_CategoryId;
	}

	public Set<Book> getShop_homeappl_CategoryId() {
		return shop_homeappl_CategoryId;
	}

	public void setShop_homeappl_CategoryId(Set<Book> shop_homeappl_CategoryId) {
		this.shop_homeappl_CategoryId = shop_homeappl_CategoryId;
	}
	
	
}
