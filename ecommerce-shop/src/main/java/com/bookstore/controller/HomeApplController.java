package com.bookstore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.Electronics;
import com.bookstore.entity.Health;
import com.bookstore.entity.HomeAppl;
import com.bookstore.repository.HomeApplCategoryRepository;
import com.bookstore.repository.HomeApplRepository;

@RestController
@CrossOrigin(origins = "*")
public class HomeApplController {

	@Autowired
	private HomeApplRepository homeApplRepository;
	@Autowired
	private HomeApplCategoryRepository homeApplCategoryRepository;
	
	@GetMapping("/homeapplcategories")
	public List<CategoryWithId> getHomeApplCategories() {
		return homeApplCategoryRepository.findAllWithoutHomeApplId();
	}
	
	@GetMapping("/homeappls")
	public List<HomeAppl> homeAppls() {
		return homeApplRepository.findAll();
	}
	
	@PostMapping("/homeappls")
	public void saveHomeAppl(@RequestBody HomeAppl homeappl) {
		homeApplRepository.save(homeappl);
	}
	
	@GetMapping("/homeappls/{id}")
	public List<HomeAppl> homeApplsByHomeApplCategory(@PathVariable("id") Long id) {
		return homeApplRepository.findHomeApplsByHomeCategoryId(id);
	}
	
	@GetMapping("/homeappl/{key}")
	public List<HomeAppl> homeapplsBySearchKey(@PathVariable("key") String key) {
		return homeApplRepository.searchHomeAppl(key);
	}
	
	@GetMapping("/gethomeappl/{id}")
	public Optional<HomeAppl> getHomeAppl(@PathVariable("id") Long id) {
		return homeApplRepository.findById(id);
	}
}
