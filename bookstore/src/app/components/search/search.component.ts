import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { ActivatedRoute } from '@angular/router';
import { ClothService } from 'src/app/services/cloth.service';
import { ElectronicsService } from 'src/app/services/electronics.service';
import { HealthService } from 'src/app/services/health.service';
import { HomeapplService } from 'src/app/services/homeappl.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private route: ActivatedRoute, private bookser: BookService,private routes: ActivatedRoute,
    private clothser: ClothService, private electronicserv: ElectronicsService,
    private healthserv: HealthService, private homeapplser: HomeapplService,
    private http:HttpClient) { }

  item:string;
  key:string;
  items:any;
  uid:number;
  height:number;
  width:number;

  ngOnInit() {
      this.route.paramMap.subscribe((parm)=>{
           this.key = parm.get('key');
           this.item = parm.get('item');
           this.uid = Number(parm.get('uid'));
           if(this.item == 'cloths'){
            this.height = 400;
            this.width = 150;
       }else{
         this.width = 150;
         this.height = 260;
       }
           this.call();
      })
  }

  cartItem = {
    imgUrl: "",
    product: "",
    available:"Out of stock",
    quantity: 0,
    price: 0.0,
    customer_id:0,
    itemType: "",
    itemId: 0
  }

  

  baseUrl:string = "http://localhost:8080/cart";

  call(){

    switch(this.item){

      // call method to fetch the data from database based upon key from book table

      case "books":
        this.items = [];
        this.bookser.getBooksByKey(this.key).subscribe((books)=>{
          this.items = books;
        });
        break;

        // call method to fetch the data from database based upon key from cloth table

      case "cloths":
        this.items = [];
        this.clothser.getClothsByKey(this.key).subscribe((cloths)=>{
          this.items = cloths;
        });
        break;

        // call method to fetch the data from database based upon key from electronics table

      case "electronics":
        this.items = [];
        this.electronicserv.getElectronicsByKey(this.key).subscribe((electronics)=>{
          this.items = electronics;
        }) 
         break;

         // call method to fetch the data from database based upon key from health table

      case "healths":
        this.items = [];
        this.healthserv.getHelthsByKey(this.key).subscribe((healths)=>{
          this.items = healths;
        });
        break;

        // call method to fetch the data from database based upon key from homeappls table

      case "homeappls": 
        this.items = [];  
        this.homeapplser.getHomeApplsByKey(this.key).subscribe((homeappls)=>{
          this.items = homeappls;
        });
        break;  
    }
    
  }

  addInCart(item){
    this.cartItem.imgUrl = item.imageUrl;
    if(item.unitsInStock >=0){
         this.cartItem.available = "In stock";
    }
    this.cartItem.price = item.unitPrice;
    this.cartItem.product = item.name;
    this.cartItem.quantity = 1;
    this.cartItem.customer_id = this.uid;
    this.cartItem.itemId = item.id;
    
    switch(this.item){

      case "books":
          this.cartItem.itemType = "books";
        break;

      case "cloths":
         this.cartItem.itemType = "cloths";
        break;

      case "electronics":
        this.cartItem.itemType = "electronics";
         break;

      case "healths":
        this.cartItem.itemType = "healths";
        break;

      case "homeappls": 
      this.cartItem.itemType = "homeappls";
        break;  
    }
    
   this.http.post(this.baseUrl,this.cartItem).subscribe((data)=>{
     
   });
   
 }

}
