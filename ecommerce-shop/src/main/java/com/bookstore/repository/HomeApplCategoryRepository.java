package com.bookstore.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.HomeApplCategory;

public interface HomeApplCategoryRepository extends JpaRepository<HomeApplCategory, Long> {

	@Query("SELECT new com.bookstore.dto.CategoryWithId(h.id,h.categoryName) FROM HomeApplCategory h")
	public List<CategoryWithId> findAllWithoutHomeApplId();
}
