import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { ClothService } from 'src/app/services/cloth.service';
import { ElectronicsService } from 'src/app/services/electronics.service';
import { HealthService } from 'src/app/services/health.service';
import { HomeapplService } from 'src/app/services/homeappl.service';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private cartserv: CartService, private http: HttpClient,
    private bookser: BookService,private routes: ActivatedRoute,
    private clothser: ClothService, private electronicserv: ElectronicsService,
    private healthserv: HealthService, private homeapplser: HomeapplService,
     private genserv: GeneralService, private router: Router) { }
  
  items:any;
  uid:number;
  dicremented:boolean = false;
  incremented:boolean = false;
  baseUrl:string = "http://localhost:8080/cart";
  subTotal:number = 0;
  total:number = 0;
  shipingCost:number = 0;
  shopItem:any;
 

  ngOnInit() {
      this.routes.paramMap.subscribe((parm)=>{
        this.uid = Number(parm.get('uid'));
      }); 

      this.cartserv.getItems(this.uid).subscribe((data)=>{
        this.items = data;
        this.subTotalM();
      });
      
      

  }

  subTotalM(){
    for(let data of this.items){
       this.subTotal += Number(data.price*data.quantity);
      
    }

    if(this.subTotal<300){
      this.shipingCost = 120;
    }else if(this.subTotal>300 && this.subTotal<500){
      this.shipingCost = 50;
    }else{
      this.shipingCost = 0;
    }

    this.total = this.subTotal + this.shipingCost;
  }

  dicrement(item){
    if(item.quantity>0){
        this.dicremented = true;
        item.quantity -= 1;
    }

    if(this.dicremented){
        this.http.post(this.baseUrl,item).subscribe((data)=>{
        });
        this.subTotal = 0;
        this.subTotalM();
    }
  }

  increment(item){
    if(item.quantity<10){
      this.incremented = true;
      item.quantity += 1;
  }

  if(this.incremented){
      this.http.post(this.baseUrl,item).subscribe((data)=>{
      });
      this.subTotal = 0;
      this.subTotalM();
  }
  }

  delete(item){
    this.http.delete("http://localhost:8080/cart/" + item.id).toPromise().then(()=>{
       this.cartserv.getItems(this.uid).subscribe((data)=>{
         this.items = data;
         this.subTotal = 0;
         this.subTotalM();
       })
    })
  }


  checkout(){

    for(let item of this.items){
      switch(item.itemType){

        case "books":
             this.bookser.getBookById(item.itemId).subscribe((data)=>{
               this.shopItem = data;
               this.shopItem.unitsInStock -= item.quantity;
               this.bookser.saveBook(this.shopItem);
               this.http.delete("http://localhost:8080/cart/" + item.id).toPromise().then(()=>{

               });
             });
            
          break;
  
        case "cloths":
          this.clothser.getClothById(item.itemId).subscribe((data)=>{
            this.shopItem = data;
            this.shopItem.unitsInStock -= item.quantity;
            this.clothser.saveCloth(this.shopItem);
            this.http.delete("http://localhost:8080/cart/" + item.id).toPromise().then(()=>{

               });
          });
           
          break;
  
        case "electronics":
          this.electronicserv.getElectronicById(item.itemId).subscribe((data)=>{
            this.shopItem = data;
            this.shopItem.unitsInStock -= item.quantity;
            this.electronicserv.saveElectronic(this.shopItem);
            this.http.delete("http://localhost:8080/cart/" + item.id).toPromise().then(()=>{

               });
           
          });
          
           break;
  
        case "healths":
          this.healthserv.getHelthById(item.itemId).subscribe((data)=>{
            this.shopItem = data;
            this.shopItem.unitsInStock -= item.quantity;
            this.healthserv.saveHealth(this.shopItem);
            this.http.delete("http://localhost:8080/cart/" + item.id).toPromise().then(()=>{

               });
          });
          
          
          break;
  
        case "homeappls": 
        this.homeapplser.getHomeApplById(item.itemId).subscribe((data)=>{
          this.shopItem = data;
          this.shopItem.unitsInStock -= item.quantity;
          this.homeapplser.saveHomeAppl(this.shopItem);
          this.http.delete("http://localhost:8080/cart/" + item.id).toPromise().then(()=>{

               });

        });
        
        
          break;  
      }
    }

    this.router.navigate(["/items",this.uid,"order"]);
  }

  Continue(){
    this.router.navigate(["/items",this.uid]);
  }
   
  
  

}
