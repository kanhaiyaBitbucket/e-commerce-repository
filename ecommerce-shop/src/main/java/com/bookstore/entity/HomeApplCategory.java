package com.bookstore.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="home_appl_category")
public class HomeApplCategory {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="category_name")
	private String categoryName;
	
	@OneToMany(cascade=CascadeType.ALL, targetEntity = HomeAppl.class)
	@JoinColumn(name="category_id", referencedColumnName = "id")
	private Set<Book> homeAppls;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Set<Book> getHomeAppls() {
		return homeAppls;
	}

	public void setHomeAppls(Set<Book> homeAppls) {
		this.homeAppls = homeAppls;
	}
	
	
}
