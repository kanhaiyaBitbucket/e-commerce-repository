package com.bookstore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.dto.CategoryWithId;
import com.bookstore.entity.Book;
import com.bookstore.entity.Cloth;
import com.bookstore.repository.ClothCategoryRepository;
import com.bookstore.repository.ClothRepository;

@RestController
@CrossOrigin(origins = "*")
public class ClothController {

	@Autowired
	private ClothRepository clothRepository;
	@Autowired
	private ClothCategoryRepository clothCategoryRepository;
	
	@GetMapping("/clothcategories")
	public List<CategoryWithId> getClothCategories() {
		return clothCategoryRepository.findAllWithoutClothId();
	}
	
	@GetMapping("/cloths")
	public List<Cloth> books() {
		return clothRepository.findAll();
	}
	
	@PostMapping("/cloths")
	public void saveCloth(@RequestBody Cloth cloth) {
		clothRepository.save(cloth);
	}
	
	@GetMapping("/cloths/{id}")
	public List<Cloth> clothsByClothCategory(@PathVariable("id") Long id) {
		return clothRepository.findClothByClothCategoryId(id);
	}
	
	@GetMapping("/cloth/{key}")
	public List<Cloth> clothsBySearchKey(@PathVariable("key") String key) {
		return clothRepository.searchCloths(key);
	}
	
	@GetMapping("/getcloth/{id}")
	public Optional<Cloth> getCloth(@PathVariable("id") Long id) {
		return clothRepository.findById(id);
	}
}
