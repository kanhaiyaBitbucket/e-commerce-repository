import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private http: HttpClient, private genserv : GeneralService) { }

  private baseUrl:string = "http://localhost:8080/users";

  ngOnInit() {

  
  }

  phoneCode:string[] = ["+91","+972","+198","+701"];
  code:string ="+91";
  done:boolean = false;
  user={
    name: "kanhaiya kumar",
    email: "kanhaiya.me@yahoo.com",
    contact: "9035368915",
    jobtype: "job",
    password:"kan@266368",
  }

  onSubmit(){
     this.http.post(this.baseUrl,this.user).subscribe((data)=>{
       this.done = true;
     })
  }

}
