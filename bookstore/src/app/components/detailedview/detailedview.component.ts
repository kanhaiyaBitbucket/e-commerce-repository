import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { ClothService } from 'src/app/services/cloth.service';
import { ElectronicsService } from 'src/app/services/electronics.service';
import { HomeapplService } from 'src/app/services/homeappl.service';
import { HealthService } from 'src/app/services/health.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detailedview',
  templateUrl: './detailedview.component.html',
  styleUrls: ['./detailedview.component.css']
})
export class DetailedviewComponent implements OnInit {

  constructor(private bookser: BookService,private routes: ActivatedRoute,
    private clothser: ClothService, private electronicserv: ElectronicsService,
    private healthserv: HealthService, private homeapplser: HomeapplService,
    private http: HttpClient ) { }

  id:number;
  suburl:string;  
  item:any;
  uid:number;
  height:number;
  width:number;
  
  cartItem = {
    imgUrl: "",
    product: "",
    available:"",
    quantity: 0,
    price: 0.0,
    customer_id:0,
    itemType: "",
    itemId:0
  }

  

  baseUrl:string = "http://localhost:8080/cart";

  ngOnInit() {
    this.routes.paramMap.subscribe((parm)=>{
      this.id = Number(parm.get('id'));
      this.uid = Number(parm.get('uid'));
      this.suburl = parm.get('item');
      if(this.suburl == 'cloths'){
        this.height = 500;
        this.width = 250;
   }else{
     this.width = 200;
     this.height = 300;
   }
      this.call();
    })
  }

  // call method to fetch the data from database based upon id

  call(){

    switch(this.suburl){

      // call method to fetch the data from database based upon id from book table

      case "books":
        this.item = null;
        this.bookser.getBookById(this.id).subscribe((book)=>{
          this.item = book;
        });
        break;

        // call method to fetch the data from database based upon id from cloth table

      case "cloths":
        this.item = null;
        this.clothser.getClothById(this.id).subscribe((cloth)=>{
          this.item = cloth;
        });
        break;

        // call method to fetch the data from database based upon id from electronics table

      case "electronics":
        this.item = null;
        this.electronicserv.getElectronicById(this.id).subscribe((electronic)=>{
          this.item = electronic;
        }) 
         break;

         // call method to fetch the data from database based upon id from health table

      case "healths":
        this.item = null;
        this.healthserv.getHelthById(this.id).subscribe((health)=>{
          this.item = health;
        });
        break;

        // call method to fetch the data from database based upon id from homeappls table

      case "homeappls": 
        this.item = null;  
        this.homeapplser.getHomeApplById(this.id).subscribe((homeappl)=>{
          this.item = homeappl;
        });
        break;  
    }
    
  }

  addInCart(item){
    this.cartItem.imgUrl = item.imageUrl;
    if(item.unitsInStock >0){
         this.cartItem.available = "In stock";
    }else{
      this.cartItem.available = "out of stock";
     }

    this.cartItem.price = item.unitPrice;
    this.cartItem.product = item.name;
    this.cartItem.quantity = 1;
    this.cartItem.customer_id = this.uid;
    this.cartItem.itemId = item.id;

    switch(this.suburl){

      case "books":
          this.cartItem.itemType = "books";
        break;

      case "cloths":
         this.cartItem.itemType = "cloths";
        break;

      case "electronics":
        this.cartItem.itemType = "electronics";
         break;

      case "healths":
        this.cartItem.itemType = "healths";
        break;

      case "homeappls": 
      this.cartItem.itemType = "homeappls";
        break;  
    }
    
    
   this.http.post(this.baseUrl,this.cartItem).subscribe((data)=>{
     
   });
   
 }

}
