import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../commom/book';
import { BookCategory } from '../commom/BookCategory';


@Injectable({
  providedIn: 'root'
})
export class HealthService {

  constructor(private http: HttpClient) { }


  private baseUrl:string = "http://localhost:8080/healths";
  private bookCategoryUrl:string = "http://localhost:8080/healthcategories";

// method for  Geting all Helths

  getHelths():Observable<Book[]>{
    return this.http.get<Book[]>(this.baseUrl)
  }

// method for  Geting all HelthsCategory

  getHelthsCategory():Observable<BookCategory[]>{
    return this.http.get<BookCategory[]>(this.bookCategoryUrl)
  }

//  method for  Geting  Helths based upon HelthsCategory_id

  getHelthsById(id):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/healths/" + id);
  }

//  method for  Geting  Helths based upon search_key

  getHelthsByKey(key):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/health/" + key);
  }

  //  method for  Geting  Helths based upon HelthsCategory_id

  getHelthById(id):Observable<Book>{
    return this.http.get<Book>("http://localhost:8080/gethealth/" + id);
  }

  saveHealth(health){
    this.http.post(this.baseUrl,health).subscribe((data)=>{ })
}

}
