import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Item } from '../commom/cart';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "http://localhost:8080/cart";

  public getItems(id:number):Observable<Item[]>{
    return this.http.get<Item[]>("http://localhost:8080/cart/" + id);
  }

}
