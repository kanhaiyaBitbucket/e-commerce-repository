package com.bookstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bookstore.dto.User;
import com.bookstore.entity.Customer;
import com.bookstore.repository.CustomerRepository;

@RestController
@CrossOrigin(origins = "*")
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepo;
	
	@GetMapping("/users")
	public List<Customer> getCustomers(){
		return customerRepo.findAll();
	}
	
	@PostMapping("/users")
	public void setCustomers(@RequestBody Customer cus){
		 customerRepo.save(cus);
	}
	
	@GetMapping("/users/{email}/{password}")
	public User getCustomer(@PathVariable("email") String email,@PathVariable("password") String password){
		return customerRepo.findByEmailAndPassword(email, password);
				
	}
}
