package com.bookstore.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CategoryWithId {

	private Long id;
	private String categoryName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public CategoryWithId(Long id, String categoryName) {
		super();
		this.id = id;
		this.categoryName = categoryName;
	}
	public CategoryWithId() {
		super();
	}
	
	
}
