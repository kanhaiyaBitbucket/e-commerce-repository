import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../commom/book';
import { BookCategory } from '../commom/BookCategory';

@Injectable({
  providedIn: 'root'
})
export class HomeapplService {

  constructor(private http: HttpClient) { }

  private baseUrl:string = "http://localhost:8080/homeappls";
  private bookCategoryUrl:string = "http://localhost:8080/homeapplcategories";

// method for  Geting all homeappls

  getHomeAppls():Observable<Book[]>{
    return this.http.get<Book[]>(this.baseUrl)
  }

// method for  Geting all homeapplsCategory

  getHomeApplsCategory():Observable<BookCategory[]>{
    return this.http.get<BookCategory[]>(this.bookCategoryUrl)
  }

//  method for  Geting  homeappls based upon homeapplsCategory_id

  getHomeApplsById(id):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/homeappls/" + id);
  }

//  method for  Geting  homeappls based upon search_key

  getHomeApplsByKey(key):Observable<Book[]>{
    return this.http.get<Book[]>("http://localhost:8080/homeappl/" + key);
  }

//  method for  Geting  homeappls based upon id

  getHomeApplById(id):Observable<Book>{
    return this.http.get<Book>("http://localhost:8080/gethomeappl/" + id);
  }

  saveHomeAppl(homeappl){
    this.http.post(this.baseUrl,homeappl).subscribe((data)=>{ })
}

}
